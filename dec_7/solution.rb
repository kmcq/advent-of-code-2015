class Circuit
  attr_accessor :instructions, :wires

  def initialize(instructions)
    self.instructions = instructions.map { |i| Instruction.new(i) }
    self.wires = {}
  end

  def value_of_wire(wire_name)
    instruction = find_instruction_for(wire_name)
    evaluate_instruction(instruction)
  end

  private

  def find_instruction_for(wire)
    instructions.find do |instruction|
      instruction.target == wire
    end
  end

  # This should probably be in the Instruction class?
  def evaluate_instruction(instruction)
    instruction.inputs.map! do |wire|
      if is_number?(wire)
        wire
      elsif wires.key?(wire)
        wires[wire]
      else
        inst = find_instruction_for(wire)
        evaluate_instruction(inst)
      end
    end
    wires[instruction.target] = instruction.evaluate
  end

  def is_number?(string)
    true if Float(string) rescue false
  end
end

class Instruction
  OPERATORS = {
    AND: '&',
    OR: '|',
    NOT: '~',
    RSHIFT: '>>',
    LSHIFT: '<<',
  }

  FRONT_OPERATORS = [
    OPERATORS[:NOT]
  ]

  SET_VALUE = :set_value

  attr_accessor :operator, :inputs, :target

  def initialize(instruction_string)
    # Examples of instruction types:
    # 0 -> c
    # af AND ah -> ai
    # NOT lk -> ll
    # hz RSHIFT 1 -> is
    instruction_parts = instruction_string.chomp.split(' ')
    if (operator = OPERATORS[instruction_parts.first.to_sym])
      self.operator = operator
      self.inputs = [instruction_parts[1]]
      self.target = instruction_parts.last
    elsif instruction_parts.size == 3
      self.operator = SET_VALUE
      self.inputs = [instruction_parts.first]
      self.target = instruction_parts.last
    else
      self.inputs = instruction_parts.values_at(0, 2)
      self.operator = OPERATORS[instruction_parts[1].to_sym]
      self.target = instruction_parts[4]
    end
  end

  def evaluate
    if operator == SET_VALUE
      inputs.first
    elsif FRONT_OPERATORS.include?(operator)
      eval("#{operator}#{inputs.first}")
    else
      eval("#{inputs.join("#{operator}")}")
    end
  end
end

circuit = Circuit.new(File.readlines(ARGV[0]))
puts circuit.value_of_wire('a')
