surface_area = 0
ribbon_length = 0
File.foreach(ARGV[0]) do |dimensions|
  # Force l and w to always be the smallest dimensions
  l, w, h = dimensions.chomp.split('x').map(&:to_i).sort
  surface_area += (2 * l * w) + (2 * w * h) + (2 * h * l) + (l * w)
  ribbon_length += (2 * l + 2 * w) + (l * w * h)
end
puts "Amount for wrapping paper: #{surface_area}"
puts "Amount for ribbon: #{ribbon_length}"
