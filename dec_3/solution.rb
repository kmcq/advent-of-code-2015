instructions = File.read(ARGV[0])
decode_instruction = {
  '^' => [0, 1],
  'v' => [0, -1],
  '<' => [-1, 0],
  '>' => [1, 0],
}

current_location_santa = [0, 0]
current_location_robosanta = [0, 0]

def move_santa(location1, location2)
  [location1[0] + location2[0], location1[1] + location2[1]]
end

i = 0
locations = instructions.chomp.split(//).inject([]) do |arr, instruction|
  decoded_instruction = decode_instruction[instruction]
  if i.even?
    new_location = move_santa(current_location_santa, decoded_instruction)
    current_location_santa = new_location
  else
    new_location = move_santa(current_location_robosanta, decoded_instruction)
    current_location_robosanta = new_location
  end
  i += 1
  arr << new_location
end
locations << [0, 0]

puts "Solution 2 #{locations.uniq.length}"
