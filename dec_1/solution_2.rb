file = ARGV[0]
text = File.read(file)
current_floor = 0
position = 0

while current_floor >= 0
  instruction = text[position]
  if instruction == '('
    current_floor += 1
  else
    current_floor -= 1
  end
  position += 1
end

puts "He went into the basement at position #{position}."
