file = ARGV[0]
text = File.read(file)
up_floors = text.gsub(')', '').length
down_floors = text.gsub('(', '').length
puts "Santa travels up #{up_floors}."
puts "Santa travels down #{down_floors}."
puts "So he arrives at floor #{up_floors - down_floors}."
